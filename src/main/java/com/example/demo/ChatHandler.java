package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;

@Component
public class ChatHandler {

    protected static String RESPONSE_TEXT= "Hello Reactive People!";
    protected static String CHAT_EXCHANGE_NAME="chat";
    protected String rabbitUsername="admin";
    protected String rabbitPassword="admin";
    protected String rabbitVirtualHost="/";
    protected String rabbitHostname="localhost";
    protected int rabbitPort=5672;
    protected ConnectionFactory rabbitFactory;
    protected CompletableFuture<Connection> rabbitFutureConnection;
    protected Connection rabbitConnection;
    protected CompletableFuture<Channel> chatFutureChannel;
    protected Channel chatChannel;
    protected List<String> chatQueues=Collections.synchronizedList(new LinkedList<String>());

    public ChatHandler() {
        rabbitFactory = createRabbitFactory();
    }

    public Mono<ServerResponse> begin(ServerRequest serverRequest) {
        return Mono
            .fromFuture(
                createChatQueue("test")
                .thenApply((name) -> {
                    return ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(name))
                        .block()
                        ;
                })
            )
            ;
    }

    public Mono<ServerResponse> leave(ServerRequest serverRequest) {
        return ServerResponse
            .ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue("leave"));
    }

    public Mono<ServerResponse> channels(ServerRequest serverRequest) {
        return ServerResponse
            .ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(chatQueues));
    }

    protected ConnectionFactory createRabbitFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(rabbitUsername);
        factory.setPassword(rabbitPassword);
        factory.setVirtualHost(rabbitVirtualHost);
        factory.setHost(rabbitHostname);
        factory.setPort(rabbitPort);
        return factory;
    }

    protected CompletableFuture<Connection> createRabbitConnection() {
        if (rabbitFutureConnection == null || rabbitFutureConnection.isCompletedExceptionally()) {
            return CompletableFuture
                .supplyAsync(() -> {
                    try {
                        rabbitConnection = rabbitFactory.newConnection();
                        return rabbitConnection;
                    }
                    catch (IOException exception) {
                        throw new CompletionException(exception);
                    }
                    catch (TimeoutException exception) {
                        throw new CompletionException(exception);
                    }
                })
                ;
        }
        else if (!rabbitFutureConnection.isDone()) {
            return CompletableFuture
                .supplyAsync(() -> {
                    return rabbitFutureConnection.join();
                })
                ;
        }
        else {
            return CompletableFuture
                .completedFuture(rabbitConnection);
        }
    }

    protected CompletableFuture<Channel> createRabbitChannel() {
        if (chatFutureChannel == null || chatFutureChannel.isCompletedExceptionally()) {
            return createRabbitConnection()
                .thenApplyAsync((i) -> {
                    try {
                        chatChannel = rabbitConnection.createChannel();
                        return chatChannel;
                    }
                    catch (IOException exception) {
                        throw new CompletionException(exception);
                    }
                })
                ;
        }
        else if (!chatFutureChannel.isDone()) {
            return CompletableFuture
                .supplyAsync(() -> {
                    return chatFutureChannel.join();
                })
                ;
        }
        else {
            return CompletableFuture
                .completedFuture(chatChannel);
        }
    }

    protected CompletableFuture<String> createChatQueue(String name) {
        return createRabbitChannel()
            .thenApplyAsync((i) -> {
                try {
                    chatChannel.exchangeDeclare(CHAT_EXCHANGE_NAME, "direct", false);
                    chatChannel.queueDeclare(name, false, false, true, null);
                    chatQueues.add(name);
                    return name;
                }
                catch (IOException exception) {
                    throw new CompletionException(exception);
                }
            })
            ;
    }
}
