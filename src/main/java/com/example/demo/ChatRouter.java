package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.*;

@Component
public class ChatRouter {

    @Bean
    public RouterFunction<ServerResponse> route(ChatHandler chatHandler) {
        return RouterFunctions
        .route(
            RequestPredicates.GET("/api/v1/begin").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
            serverRequest -> chatHandler.begin(serverRequest)
        )
        .andRoute(
            RequestPredicates.GET("/api/v1/leave").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
            serverRequest -> chatHandler.leave(serverRequest)
        )
        .andRoute(
            RequestPredicates.GET("/api/v1/channels").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
            serverRequest -> chatHandler.channels(serverRequest)
        )
        ;
    }
}
